#!/bin/bash5

declare -i ramdisksize # int

if [ "$1" = "" ]; then
  ramdisksize=1024 
else
  ramdisksize=$1
fi

function osxmkramdisk () {
# tested in High Sierra osx 10.13
# REF: https://stackoverflow.com/questions/46224103/create-apfs-ram-disk-on-macos-high-sierra
# REF for previous versions of OSX: https://osxdaily.com/2007/03/23/create-a-ram-disk-in-mac-os-x/
  [ $ramdisksize = 0 ] && echo "! Warning - SKIPPING RAMDISK" && return;

  echo "o Creating RAMDISK of $ramdisksize MB"  
  let blox=$ramdisksize*2048
  diskisat=$(hdid -nomount ram://$blox)
  time diskutil erasedisk hfs+ ramdisk $diskisat || echo "o Warning code 120 - Failed to create ramdisk"
# /dev/disk9s1    hfs   1.1G   13M  1.1G   2% /Volumes/ramdisk
# df -k
#Filesystem        1024-blocks      Used Available Capacity iused      ifree %iused  Mounted on
#/dev/disk9s1          1097688     12336   1085352     2%       4 4294967275    0%   /Volumes/ramdisk

# NOTE - To destroy the RAM disk again, call 
# diskutil eject <output path of previous hdid command> # e.g. diskutil eject /dev/disk2
# umount /Volumes/ramdisk
# hdiutil detach /dev/diskX
}

if [ "$1" = "0" ]; then
  d2e=$(df |awk '/ramdisk/ {print $1}')
  df -h|grep ramdisk
  echo "De-allocating RAMdisk $d2e"
#  diskutil eject $d2e
  hdiutil detach $d2e
  df -h|grep ramdisk
else
  osxmkramdisk
fi

df -h
