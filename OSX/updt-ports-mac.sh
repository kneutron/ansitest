#!/bin/bash

# macports upgrade pkgs

# FIX for: https://trac.macports.org/ticket/61337#comment:4
# no usrlocalbin
PATH=/opt/local/bin:/var/root/bin:/var/root/bin/boojum:/sbin:/root/bin:/root/bin/boojum:/usr/sbin:/bin:/usr/bin:/usr/X11R6/bin

# FIX for https://trac.macports.org/ticket/62343
mv -v /usr/local/include /usr/local/notinclude

time port selfupdate; nice port upgrade outdated

[ -e /usr/local/notinclude ] && mv -v /usr/local/notinclude /usr/local/include
date

sqlite3 /opt/local/var/macports/registry/registry.db \
 -line "SELECT ports.name,datetime(ports.date, 'unixepoch') FROM  ports  ORDER BY ports.date;" \
 |awk 'NF>0' \
 |paste - - \
 |awk '{print $1, $2, $3, $7, $8}' \
 |tee $HOME/macports-upgraded-pkgs-`date +%Y%m%d`.log

exit;

sqlite REF: https://apple.stackexchange.com/questions/420673/does-macports-keep-a-log-for-actions-like-install-and-upgrade


TODO if a port is broken - You can also specify "outdated and not foo" in the upgrade command
REF: https://apple.stackexchange.com/questions/65367/macports-continue-installing-other-updates-after-error

---

Raw sql result:
                             name = ncurses
datetime(ports.date, 'unixepoch') = 2019-11-15 04:58:41

                             name = libiconv
datetime(ports.date, 'unixepoch') = 2019-11-15 04:58:45

                             name = gettext
datetime(ports.date, 'unixepoch') = 2019-11-15 04:58:49


after paste - -
				1 2 3		4			5	  6 7		8
                             name = ncurses     datetime(ports.date, 'unixepoch') = 2019-11-15 04:58:41
                             name = libiconv    datetime(ports.date, 'unixepoch') = 2019-11-15 04:58:45
                             name = gettext     datetime(ports.date, 'unixepoch') = 2019-11-15 04:58:49
